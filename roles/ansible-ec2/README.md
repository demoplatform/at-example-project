ansible-ec2
===========

This role is to manage AWS resources.

Currently limited to single project instance, for a single monitoring EC2 instacne. 

Requirements
------------

Ensure `ansible-galaxy collection install amazon.aws` is installed.
Ensure `boto3` and `botocore` is installed. 

Role Variables
--------------


Dependencies
------------


Example Playbook
----------------


License
-------

MIT

Author Information
------------------


